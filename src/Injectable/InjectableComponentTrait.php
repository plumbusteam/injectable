<?php namespace Plumbus\Injectable;

/**
 * Class InjectableComponentTrait
 * @package Plumbus\Injectable
 */
trait InjectableComponentTrait
{
    /**
     * @return mixed
     */
    public static function instance()
    {
        return InjectableComponent::getInstance(static::class);
    }
}
