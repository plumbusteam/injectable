Dependency Injection посредством php traits

Включение компонента производится с помощью use SomeComponentTrait;
Для включаемого компонента необходимо использовать use InjectableComponentTrait - тогда компонент приобретает метод getInstance() и его экземпляр хранится в свойстве $instances класса \Plumbus\Injectable\InjectableComponent

Подробнее от автора https://docs.google.com/document/d/1ujkxg0OtwfpuPYG2atvhC9eWev06jjho5_He17JgdlU/edit?usp=sharing
Поддержка https://plumbusteam.ru/component/plumbus/injectable